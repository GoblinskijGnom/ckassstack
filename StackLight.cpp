#include <iostream>

template <typename T>
class Stack
{ 
private: 
    int size = 0;
    T* array = new T[size];
public:

    void pop()
    {
        size--;
        T* NewArray = new T[size];
        for (int i = 0; i < size; i++)
        {
            NewArray[i] = array[i];
        }
        delete[] array;
        array = NewArray;
    }
    void push(T x)
    {
       T* NewArray = new T[size + 1];
        for (int i = 0; i < size; i++)
        {
            NewArray[i] = array[i];
        }
        NewArray[size] = x;
        size++;
        delete[] array;
        array = NewArray;
    }
    void Showarray()
    {
        for (int i = 0; i < size; i++)
        {
            std::cout << array[i] << '\t';
        }
        std::cout << '\n';
    }
};

int main()
{
    Stack<int> Ex;
    Stack<std::string> Names;
    Names.push("Ilja");
    Names.Showarray();
    Names.pop();
    Ex.push(6);  
    Ex.push(3);
    Ex.Showarray();
    Ex.pop();
    Ex.Showarray();
    Ex.pop();

}